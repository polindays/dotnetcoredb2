﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPagesMovie.Pages
{
    public class tmpModel : PageModel
    {
        public string GetNonSensitiveDataById()
        {

            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-0GPKG3Q\\MSSQLSERVER2017;Database=RazorPagesMovieContext-996922cf-5f8a-434f-b5fc-bffaa6eada10;Trusted_Connection=True;MultipleActiveResultSets=true"))
            {
                connection.Open();
                SqlCommand command = new SqlCommand($"SELECT * FROM Movie WHERE Id =2", connection);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string returnString = string.Empty;
                        returnString += $"Title : {reader["Title"]}.";
                        returnString += $"Genre : {reader["Genre"]}";
                        return returnString;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
        }
    }
}